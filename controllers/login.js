const bcrypt = require('bcryptjs');
const User = require("../models/user");
const { Op } = require("sequelize");

exports.loginPage = (req, res, next) => {
    if (req.session.isLoggedIn) {
        res.redirect('/');
    } else {
        res.render('login', { pageTitle: 'Login', user: req.session.user });
    }
};

exports.loginPost = (req, res, next) => {
    const { emailOrUsername, password } = req.body;
    User.findOne({
        where: {
            [Op.or]: [{ email: emailOrUsername }, { username: emailOrUsername }]
        }
    })
        .then(user => {
            if (!user) {
                return res.redirect('/login');
            }
            bcrypt
                .compare(password, user.pass)
                .then(matches => {
                    if (matches) {
                        req.session.isLoggedIn = true;
                        req.session.user = user;
                        return res.redirect('/');
                    }
                    res.redirect('/login');
                })
                .catch(err => {
                    console.log(err);
                    res.redirect('/login');
                });
        })
        .catch(err => console.log(err));
};


exports.logoutPost = (req, res, next) => {
    req.session.destroy(err => {
        console.log(err);
        res.redirect('/login');
    });
};