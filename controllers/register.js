const bcrypt = require('bcryptjs');
const User = require("../models/user");

exports.registerPage = (req, res, next) => {
    let user = req.session.user ? req.session.user : {};

    res.render('register', { pageTitle: 'Register', validation: {}, validation: {}, values: { first_name: "", last_name: "", username: "", email: "" } , user});
};



const validateLettersNumbersOnly = (toValidate) => {
    for (let i = 0; i < toValidate.length; i++) {
        let totest = toValidate.charCodeAt(i);
        if (!((totest >= 48 && totest <= 57) ||
            (totest >= 65 && totest <= 90) ||
            (totest >= 97 && totest <= 122))) {
            return false;
        }
    }
    return true;
}

exports.handleRegisterPost = (req, res, next) => {
    /* destructuring needed variables from req.body (post data) */
    const { first_name, last_name, username, email, pass1, pass2 } = req.body;

    /*  Validate things, needs more work.... */
    let validation = {};
    validation.first_name = !validateLettersNumbersOnly(first_name);
    validation.last_name = !validateLettersNumbersOnly(last_name);
    validation.username = !validateLettersNumbersOnly(username);
    // validation.email = !validateLettersNumbersOnly(email);
    validation.pass1 = pass1 !== pass2;
    // let pass1 = validateLettersNumbersOnly(pass1);
    validation.pass2 = pass1 !== pass2;

    /*  Loop through all validations and if anything is off, reload the page but keep values  */
    let invalidEntires = false;
    Object.entries(validation).forEach(([key, invalid]) => {
        invalidEntires |= invalid;
    });
    if (invalidEntires) {
        let user = req.session.user ? req.session.user : {};
        res.render('register', { pageTitle: 'Register', validation, values: { first_name, last_name, username, email }, user });
        return;
    }

    /*  all is good! add the user to the database  */
    return bcrypt
        .hash(pass1, 12)
        .then(hashedPassword => {
            User.create({
                first_name,
                last_name,
                username,
                email,
                pass: hashedPassword
            })
                .then(() => {
                    console.log("Successfully created user");
                    res.redirect('/login');
                })
                .catch(() => {
                    console.log("Error with creating user");
                    let user = req.session.user ? req.session.user : {};
                    res.render('register', {
                        pageTitle: 'Register',
                        validation: {}, values: { first_name: "", last_name: "", username: "", email: "" }, user
                    });
                })
        });

};
