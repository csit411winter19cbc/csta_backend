
exports.main = (req, res, next) => {
    res.render('csta_home',
        {
            pageTitle: 'CSTA Home',
            user: req.session.user,
            links: [
                {
                    href: "/",
                    text: "Home",
                    src: "/images/home-3-256.png"
                }
            ]
        }
    );
};
