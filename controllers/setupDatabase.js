const allModels = require("../models/index");
const sequelize = require("../util/database");


exports.updateDatabase = (req, res, next) => {
    /*  might be useful code later, or for other types of index files, but the code after this commented out code works better here */
    // Object.entries(allModels).forEach(([modelName, model]) => {
    //     model.sync()
    //         .then(()=>{console.log("Successfully synced model(aka table): ", modelName)})
    //         .catch(()=>{console.log("Error with syncing model(aka table): ", modelName)})
    // });
    sequelize.sync()
        .then(() => {
            console.log("Successfully synced models(aka tables): ");
            res.redirect('back');
        })
        .catch(() => { console.log("Error with syncing models(aka tables)") })

};