const Sequelize = require("sequelize");

const sequelizeInstance = require("../util/database");

const User = sequelizeInstance.define("user", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  type: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  username: {
    type: Sequelize.STRING(30),
    allowNull: false
  },
  email: {
    type: Sequelize.STRING(80),
    allowNull: false
  },
  pass: {
    type: Sequelize.STRING(255),
    allowNull: false
  },
  first_name: {
    type: Sequelize.STRING(20),
    allowNull: false
  },
  last_name: {
    type: Sequelize.STRING(40),
    allowNull: false
  },
  date_expires: {
    type: Sequelize.DATE,
    defaultValue: null
  }
});

module.exports = User;


// CREATE TABLE users (
// 	id INT PRIMARY KEY AUTO_INCREMENT,
// 	/*
// 		type:
// 		0 - user
// 		1 - admin
// 	*/
// 	`type` INT NOT NULL DEFAULT 0,
// 	username VARCHAR(30) NOT NULL,
// 	email VARCHAR(80) NOT NULL,
// 	pass VARCHAR(255) NOT NULL,
// 	first_name VARCHAR(20) NOT NULL,
// 	last_name VARCHAR(40) NOT NULL,
// 	date_expires DATE DEFAULT NULL,

  /*  These are auto generated by Sequelize */
// 	date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
// 	date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
// );