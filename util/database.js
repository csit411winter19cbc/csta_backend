const Sequelize = require("sequelize");

const parsed = process.env.MYSQLCONNSTR_localdb.split(";");
let variables = []
parsed.forEach((str, i)=>{
    //console.log(i, str);
    variables[i] = str.split("=")[1];
})

const sequelize = new Sequelize("csta", variables[2], variables[3], {
    dialect: "mysql",
    host: variables[1].split(":")[0],
    port: variables[1].split(":")[1]
});

module.exports = sequelize;
