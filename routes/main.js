var express = require('express');
var router = express.Router();
const mainController = require('../controllers/main');
const auth = require('../middlewares/auth');


/* GET home page. */
router.get('/', auth, mainController.main);

module.exports = router;
