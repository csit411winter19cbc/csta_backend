var express = require('express');
var router = express.Router();


const registerController = require('../controllers/register');

/* GET for test controller. */
router.get('/register', registerController.registerPage);

router.post('/register', registerController.handleRegisterPost);

module.exports = router;
