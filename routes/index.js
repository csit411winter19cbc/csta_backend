exports.addRoutes = (app) => {

  let mainRouter = require('./main');
  app.use(mainRouter);

  let setupDatabaseRouter = require('./setupDatabase');
  app.use(setupDatabaseRouter);

  let loginRouter = require('./login');
  app.use(loginRouter);

  let logoutRouter = require('./logout');
  app.use(logoutRouter);

  let registerRouter = require('./register');
  app.use(registerRouter);
}