var express = require('express');
var router = express.Router();
const auth = require('../middlewares/auth');

const setupDatabaseController = require('../controllers/setupDatabase');

/* GET for test controller. */
router.get('/setupdatabase', auth, setupDatabaseController.updateDatabase);

module.exports = router;
