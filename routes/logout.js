var express = require('express');
var router = express.Router();

const loginController = require('../controllers/login');

/* GET for test controller. */
router.get('/logout', loginController.logoutPost);

router.post('/logout', loginController.logoutPost);

module.exports = router;
