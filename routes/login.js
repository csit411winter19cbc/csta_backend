var express = require('express');
var router = express.Router();

const loginController = require('../controllers/login');

/* GET for test controller. */
router.get('/login', loginController.loginPage);

router.post('/login', loginController.loginPost);

module.exports = router;
